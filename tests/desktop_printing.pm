use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;
    my $user_home = "/home/" . get_var("USER_LOGIN", "testy");
    my $pdf_dest = $user_home . "/" . ( check_var("DISTRI", "fedora") ? "Desktop" : "PDF" );

    # throw a delay in, to ensure that we've a settled state in the desktop
    # before switching away from it, in the hope that lets us get back here
    wait_still_screen(stilltime=>5, similarity_level=>45);

    # Prepare the environment:
    #
    # Become root
    $self->root_console(tty=>3);
    console_loadkeys_us;

    # Create a text file with content to print
    script_run  "cd $user_home";
    assert_script_run  "echo 'A quick brown fox jumps over a lazy dog.' > testfile.txt";
    script_run "chmod 666 testfile.txt";
    # Install the Cups-PDF package to use the Cups-PDF printer
    assert_script_run "apt install -y printer-driver-cups-pdf", 180;
    # Leave the root terminal and switch back to desktop.
    desktop_vt();
    my $desktop = get_var("DESKTOP");
    # some simple variances between desktops. defaults are for GNOME
    my $editor = "gedit" ;
    # in Debian we switched to gnome-text-editor with GNOME v42
    $editor = "gnome-text-editor" if (get_var("GNOMEVERSION") >= 42);
    my $viewer = "evince";
    my $maximize = "super-up";
    if ($desktop eq "kde") {
        $editor = "kwrite";
        $viewer = "okular";
        $maximize = "super-pgup";
    }

    # Open the text editor and print the file.
    wait_screen_change { send_key "alt-f2"; };
    wait_still_screen(stilltime=>5, similarity_level=>45);
    type_very_safely "$editor $user_home/testfile.txt\n";
    wait_still_screen(stilltime=>5, similarity_level=>44);
    if (check_screen "console_instead_of_GUI", 1) {
        die "failed to switch back to GUI";
    }
    # Print the file using the Cups-PDF printer
    wait_screen_change { send_key "ctrl-p"; };
    wait_still_screen(stilltime=>5, similarity_level=>45);
    #if ($desktop eq 'gnome') {
    #    assert_and_click "printing_select_pdfprinter";
    #}
    #else {
        # It seems that on newly installed KDE systems with no
        # printer,  the Cups-PDF printer is already pre-selected.
        # We only check that it is correct.
        assert_screen "printing_pdfprinter_ready";
    #}
    wait_still_screen(stilltime=>2, similarity_level=>45);
    assert_and_click "printing_print";
    # Exit the application
    send_key "alt-f4";
    # Wait out confirmation on GNOME
    if (check_screen "printing_print_completed", 1) {
        sleep 30;
    }

    # Open the pdf file and check the print
    send_key "alt-f2";
    wait_still_screen(stilltime=>5, similarity_level=>45);
    type_safely "bash -c '$viewer $pdf_dest/*-job_1.pdf'\n";
    wait_still_screen(stilltime=>5, similarity_level=>45);
    # Resize the window, so that the size of the document fits the bigger space
    # and gets more readable.
    send_key $maximize;
    wait_still_screen(stilltime=>2, similarity_level=>45);
    # make sure we're at the start of the document
    send_key "ctrl-home" if ($desktop eq "kde");
    # Check the printed pdf.
    assert_screen "printing_check_sentence";
}


sub test_flags {
    return { fatal => 1 };
}

1;

# vim: set sw=4 et:
