use base "installedtest";
use strict;
use testapi;
use utils qw(console_loadkeys_us make_sure_curl_is_available);

sub run {
    my $self = shift;
    $self->root_console(tty=>3);
    # let's see if this fixes the @ vs. " typing issue...
    console_loadkeys_us;

    if (make_sure_curl_is_available()) {
        assert_script_run 'top -i -n20 -b > /var/tmp/top.txt', 120;
        upload_logs '/var/tmp/top.txt';
        unless (get_var("CANNED")) {
            assert_script_run 'dpkg -l > /var/tmp/debs.txt', 80;
            upload_logs '/var/tmp/debs.txt';
        }
        assert_script_run 'free > /var/tmp/free.txt';
        upload_logs '/var/tmp/free.txt';
        assert_script_run 'df > /var/tmp/df.txt';
        upload_logs '/var/tmp/df.txt';
        assert_script_run 'systemctl -t service --no-pager --no-legend | grep -o "[[:graph:]]*\.service" > /var/tmp/services.txt';
        upload_logs '/var/tmp/services.txt';
        assert_script_run 'ls -l /var/log /var/log/journal || true';
        upload_logs '/var/log/syslog', log_name => '_collect_data-syslog.txt';
        assert_script_run 'head /etc/apt/sources.list /etc/apt/sources.list.d/* > /var/tmp/apt-sources-lists.txt || true';
        upload_logs '/var/tmp/apt-sources-lists.txt';
    }
}

sub test_flags {
    return { 'ignore_failure' => 1 };
}

1;

# vim: set sw=4 et:
