# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C) 2017-2021 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;
	assert_screen 'DeviceToUseAsRoot';
    send_key 'down';
    wait_still_screen 2;
    save_screenshot;
    send_key 'ret';

    my $tries = 10;
    while (1) {
        $tries -= 1;
        die "We seem to be getting nowhere here, giving up" if ($tries < 0);

        assert_screen [qw(RescueOperations MountFailed MountEfiPartition)];
        if (match_has_tag('MountEfiPartition')) {
            # send_key get_var("TEXT_MODE") ? 'right' : 'down'; # select No
            send_key 'ret';
        }
        elsif (match_has_tag('MountFailed')) {
            send_key 'ret';
            assert_screen 'DeviceToUseAsRoot';
            send_key 'up';
            wait_still_screen 2;
            save_screenshot;
            send_key 'ret';
        }
        elsif (match_has_tag('RescueOperations')) {
            last;
        }
        else {
            die "ERROR: unreachable else reached"
        }
    }

    # try a d-i shell first, in case the target is a mess and breaks things
	send_key 'down';
    wait_still_screen 1;
    save_screenshot;
	send_key 'ret';
    assert_screen 'ConfirmShell';
	send_key 'ret';
	assert_screen 'RootPrompt';
    type_string "df -h\n";
    type_safely "exit";
    save_screenshot;
	send_key 'ret';
	assert_screen 'RescueOperations';

    # this time, we go back up to select the in-target shell
	send_key 'up';
    wait_still_screen 1;
    save_screenshot;
	send_key 'ret';
    assert_screen 'ConfirmShell';
	send_key 'ret';
	assert_screen 'RootPrompt';
    type_string "df -h\n";
    type_safely "exit";
    save_screenshot;
	send_key 'ret';
	assert_screen 'RescueOperations';

    # let's collect diags regardless
#    $self->post_fail_hook();
}

sub test_flags {
    return { fatal => 1 };
}

1;
# vim: set sw=4 et:
