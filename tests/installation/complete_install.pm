# Copyright (C) 2014-2017 SUSE LLC
# Copyright (C)      2017 Philip Hands
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.

use base "debianinstallertest";
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;

    # see if we can avoid the screensaver -- see #787279 and grub.pm
    mouse_set(800, 820);
    sleep 1;
    mouse_hide;

    assert_screen 'CompleteInstall', 300;

    $self->root_console(tty=>2);
    # let's see if this fixes the @ vs. " typing issue...
    console_loadkeys_us;

    script_run("gdisk -l /dev/vda > /tmp/gdisk.txt");
    script_run 'df -h > /tmp/df.txt';
    if (make_sure_curl_is_available) {
        upload_logs '/tmp/gdisk.txt';
        upload_logs '/tmp/df.txt';
        upload_logs '/var/log/Xorg.0.log', log_name => 'Xorg.0.log.txt', failok => 1;
        upload_logs '/target/etc/apt/sources.list', log_name => 'in-target_sources.list.txt';
        assert_script_run 'chroot /target od -A x -t x1z -v /dev/vda | head -1000 > /tmp/dev_vda_dump.txt';
        upload_logs '/tmp/dev_vda_dump.txt';
        upload_logs '/var/log/syslog', log_name => 'complete_install-DI_syslog.txt';
        script_run("cp /var/lib/dpkg/status /target/tmp/DI-status ; chroot /target apt -o 'Dir::State::status=/tmp/DI-status' list --installed > /tmp/DI-installed-pkgs.txt");
        upload_logs '/tmp/DI-installed-pkgs.txt';
        script_run("( set -x ; ls -ld /etc/pcm* ) > /dev/${serialdev}");
    }
    else {
        # splurge these at the serial port, to get it into the OpenQA logs without curl
        script_run("cat /tmp/gdisk.txt > /dev/${serialdev}");
        script_run("cat /tmp/df.txt > /dev/${serialdev}");
        script_run("cat /var/log/Xorg.0.log > /dev/${serialdev}");
        script_run("head -n-0 /etc/apt/sources.list /target/etc/apt/sources.list 2>/dev/null > /dev/${serialdev}");
        script_run("head -1000 /var/log/syslog > /dev/${serialdev}");
        script_run("tail -2000 /var/log/syslog > /dev/${serialdev}");
    }
    send_key ('gtk' eq (get_var('DI_UI', 'gtk')) ? "ctrl-alt-f5" : "alt-f1");  # FIXME: would be nice to make $self->desktop_vt() work here
    # make sure we're back at the right screen (seems to blank)
    assert_screen [qw(blankScreen CompleteInstall)] ;
    if (match_has_tag("blankScreen")) {
        # FIXME -- I think this should be fixed, but let's allow some nice green results for now
        # record_soft_failure 'deb#787279: a screen-saver in the installer is mostly pointless';
        mouse_set(800, 820);
        sleep 1;
        mouse_hide;
        assert_screen 'CompleteInstall';
    }

    send_key 'ret';
    assert_screen [qw(shuttingDown shutdown_removing_files rebooted)], 120 ;
    if (match_has_tag('shutdown_removing_files')) {
        assert_screen 'shuttingDown', 1200 ;
    }
    if (get_var('INSTALLONLY')) {
        assert_shutdown;
    }
}

sub test_flags {
    return { fatal => 1 };
}

1;
# vim: set sw=4 et:
