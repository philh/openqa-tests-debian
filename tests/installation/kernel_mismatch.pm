# SUSE's openQA tests
#
# Copyright © 2009-2013 Bernhard M. Wiedemann
# Copyright © 2012-2017 SUSE LLC
# Copyright ©      2017 Philip Hands
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# Summary: Bootloader to setup boot process with arguments/options
# Maintainer: Jozef Pupava <jpupava@suse.com>

use base "debianinstallertest";
use strict;
use testapi;

# this is now a separate test from hostname.pm in order to make
# kernel_mismatch errors obvious in the webUI.
sub run {
    # detectCDROM is added here to get past the 'missing CD with SCSI' problem,
    # so that this test's failures are not mesleading
    my @tags = qw(KernelMismatch EnterTheHostname EduChooseProfile detectCDROM);

    check_screen \@tags, 120;
    die "Kernel mismatch" if (match_has_tag('KernelMismatch'));
}


sub post_fail_hook {
    my $self = shift;

    $self->root_console();

    # no curl available yet, so just splurge the tail of syslog
    # at the serial port to capture the kernel version
    script_run("tail -50 /var/log/syslog > /dev/${serialdev}");
}

sub test_flags {
    return { fatal => 1 };
}

1;
# vim: set sw=4 et:
