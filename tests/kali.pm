use base "installedtest";
use strict;
use testapi;
use utils;

# we are very paranoid with waits and typing speed in this test
# because the system can be very busy; it's effectively first boot of
# a freshly installed system and we're running Firefox for the first
# time, which causes an awful lot of system load, and there's lots of
# screen change potentially going on. This makes the test quite slow,
# but it's best to be safe. If you're working on the test you might
# want to tweak the waits down a bit and use type_safely instead of
# type_very_safely for your test runs, just to save your time.

sub run {
    assert_screen 'kali_boot_menu';
    send_key 'ret';
    # check if we're going to get promted to log in
    if (grep { get_var("DESKTOP") eq $_ } qw(e17)) {
        assert_screen 'e17_lang_select', 1200;
	assert_and_click 'e17_en_UK';
    }
    my @tags = qw/e17_lang_next x_login_prompt wm_firststart kali_deskstop startup_very_slow/;
    assert_screen \@tags, 1200;
    if (match_has_tag('startup_very_slow')) {
	@tags = grep {("startup_very_slow" ne $_)} @tags;
	assert_screen \@tags, 1200;
    }
    while(match_has_tag('e17_lang_next')) {
	wait_screen_change { assert_and_click 'e17_lang_next' };
        assert_screen \@tags;
    }
    @tags = qw/x_login_prompt wm_firststart kali_deskstop/;
    if (match_has_tag('x_login_prompt')) {
        @tags = qw/wm_firststart kali_deskstop/;
        type_safely "root";
        wait_screen_change { send_key "tab"; };
        type_safely get_var("ROOT_PASSWORD", "toor");
        wait_screen_change { send_key "ret" };
        assert_screen \@tags, 1200;
    }

    if (match_has_tag('wm_firststart')) {
        assert_and_click 'wm_firststart';
    }
    assert_screen 'kali_deskstop';
}

sub test_flags {
    return { fatal => 1 };
}

1;

# vim: set sw=4 et:
