use base "debianinstallertest";
use strict;
use lockapi;
use testapi;
use utils;
use debianinstaller;
use autotest;

sub run {
    my $self = shift;
    if (get_var("PXEBOOT")) {
        # PXE tests have DELAYED_START set, so VM is not running yet,
        # because if we boot immediately PXE will time out waiting for
        # DHCP before the support server is ready. So we wait here for
        # support server to be ready, then go ahead and start the VM
        mutex_lock "support_ready";
        mutex_unlock "support_ready";
        resume_vm;
    }

    # construct the kernel params. the trick here is to wind up with
    # spaced params if GRUB or GRUBADD is set, and just spaces if not,
    # then check if we got all spaces. We wind up with a harmless
    # extra space if GRUBADD is set but GRUB is not.
    my $params = "";
    $params .= get_var("GRUB", "") . " ";
    $params .= get_var("GRUBADD", "") . " ";
    # if DEBCONF_DEBUG is set, pass that through to d-i to enable debugging
    $params .= "DEBCONF_DEBUG=5 " if get_var("DEBCONF_DEBUG");
    # if RESCUE is set, tell do_bootloader to enter rescue mode
    my $rescue = 0;
    $rescue = 1 if get_var("RESCUE");
    # if TEXT_MODE is set, tell do_bootloader to enter rescue mode
    my $textmode = 0;
    $textmode = 1 if check_var("DI_UI", "text");
    # if DU_UI=speech, tell do_bootloader to enter speech mode
    my $speech = 0;
    $speech = 1 if check_var("DI_UI", 'speech');
    # ternary: set $params to "" if it contains only spaces
    $params = $params =~ /^\s+$/ ? "" : $params;

    # set mutex wait if necessary
    my $mutex = get_var("INSTALL_UNLOCK");

    # we need a longer timeout for the PXE boot test, and UEFI can also be slow
    my $timeout = 45;
    $timeout = 90 if (get_var("UEFI"));
    $timeout = 120 if (get_var("PXEBOOT"));

    # call do_bootloader with postinstall=0, the params, and the mutex,
    # unless we're a VNC install client (no bootloader there)
    unless (get_var("VNC_CLIENT")) {
        do_bootloader(postinstall=>0, params=>$params, mutex=>$mutex, rescue=>$rescue, textmode=>$textmode, speech=>$speech, timeout=>$timeout);
    }

    # on lives, we have to explicitly launch debianinstaller
    if (get_var('LIVE')) {
        assert_and_click "live_start_debianinstaller_icon", '', 300;
    }
    my $language = get_var('LANGUAGE') || 'english';

    # wait for debianinstaller to appear
    assert_screen "debianinstaller_select_install_lang", 150;
    mouse_hide;
}


sub test_flags {
    return { fatal => 1 };
}

1;

# vim: set sw=4 et:
