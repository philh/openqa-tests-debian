package installedtest;

use strict;

use base 'basetest';

# base class for tests that run on installed system

# should be used when with tests, where system is already installed, e. g all parts
# of upgrade tests, postinstall phases...

use testapi;
use utils;

sub root_console {
    # Switch to a default or specified TTY and log in as root.
    my $self = shift;
    my %args = (
        tty => 1, # what TTY to login to
        @_);

    send_key "ctrl-alt-f$args{tty}";

    if (check_var('DISTRI', 'kali')) { # FIXME: we need a variable to indicate "sets up a root user, which kali does not"
        my $user_login = get_var("USER_LOGIN") || "testy";
        my $user_password = get_var("USER_PASSWORD") || "weakpassword";
        if (check_var('CONSOLE_AUTOLOGIN', 1)) {
            assert_screen('user_console', 30);
        }
        else {
            console_login(user => $user_login, password => $user_password);
        }
        type_safely "sudo -s\n";
        if (!check_var('CONSOLE_AUTOLOGIN', 1)) {
            # for kali at least, autologin implies passwordless sudo
            assert_screen ["sudo-passwordprompt", "root_console"], 3;
            if (match_has_tag "sudo-passwordprompt") {
                type_password $user_password;
                send_key 'ret';
            }
        }
        assert_screen "root_console", 3;
    }
    else {
        console_login;
    }
}

sub post_fail_hook {
    my $self = shift;

    if (check_screen 'emergency_rescue', 3) {
        my $password = get_var("ROOT_PASSWORD", "weakpassword");
        type_string "$password\n";
        # bring up network so we can upload logs
        assert_script_run "dhclient";
    }
    else {
        $self->root_console(tty=>6);
    }

    # kludge the keyboard to avoid typing @ instead of " and the like
    console_loadkeys_us;

    # We can't rely on tar being in minimal installs, but we also can't
    # rely on dnf always working (it fails in emergency mode, not sure
    # why), so try it, then check if we have tar
    script_run "apt -y install tar curl", 180;
    assert_script_run "dpkg -l tar";
    assert_script_run "dpkg -l curl";

#    # Note: script_run returns the exit code, so the logic looks weird.
#    # We're testing that the directory exists and contains something.
#    if (0 == script_run 'test -n "$(ls -A /var/tmp/abrt)" && cd /var/tmp/abrt && tar czvf tmpabrt.tar.gz *' // 1)) {
#        upload_logs "/var/tmp/abrt/tmpabrt.tar.gz";
#    }
#
#    if (0 == (script_run 'test -n "$(ls -A /var/spool/abrt)" && cd /var/spool/abrt && tar czvf spoolabrt.tar.gz *' // 1)) {
#        upload_logs "/var/spool/abrt/spoolabrt.tar.gz";
#    }

    # Upload /var/log
    # lastlog can mess up tar sometimes and it's not much use
    if (0 == (script_run "tar czvf /tmp/var_log.tar.gz --exclude='lastlog' /var/log" // 1)) {
        upload_logs "/tmp/var_log.tar.gz";
    }

    # Sometimes useful for diagnosing FreeIPA issues
    upload_logs "/etc/nsswitch.conf", failok=>1;
}

1;

# vim: set sw=4 et:
